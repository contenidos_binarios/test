<?php

namespace Contenidosbinarios\Test;

class Test
{
    public function saludo(String $sName)
    {
        return 'Probando, para ' . $sName . ' un paquete de Contenidos Binarios. Gracias';
    }
}